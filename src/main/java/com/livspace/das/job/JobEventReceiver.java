package com.livspace.das.job;

import com.livspace.jobs.JobApi;
import com.livspace.jobs.JobService;
import com.livspace.jobs.ServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class JobEventReceiver extends JobService{
    @Autowired
    public JobEventReceiver(Environment env) {
        ServiceConfig config = new ServiceConfig(
                env.getProperty("services.jobs.url",""),
                env.getProperty("services.jobs.authHeader", ""),
                env.getProperty("services.jobs.authToken", ""));

        String envS = env.getProperty("services.jobs.environment","development").toLowerCase();

        final JobApi api = new JobApi(config.get(envS));
        this.setApi(api);
    }
}
