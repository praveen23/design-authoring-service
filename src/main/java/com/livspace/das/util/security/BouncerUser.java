package com.livspace.das.util.security;

import com.bouncer.model.User;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class BouncerUser implements Serializable{

    private User                user;
    private Date                lastSyncWithBouncer;
    private String              accessToken;
    private int                 userId;
    private String              imageUrl;
    private Set<String>         userPermissions = new HashSet<>();


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLastSyncWithBouncer() {
        return lastSyncWithBouncer;
    }

    public void setLastSyncWithBouncer(Date lastSyncWithBouncer) {
        this.lastSyncWithBouncer = lastSyncWithBouncer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Set<String> getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(Set<String> userPermissions) {
        this.userPermissions = userPermissions;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static BouncerUser getDummyUser(){

        Set<String> permissions = new HashSet<>(); // Adding All permissions to dummy User.
        permissions.add("ADD_DP_PROFILE");
        permissions.add("APPROVE_DP_PROFILE");
        permissions.add("EDIT_DP_STATUS");
        permissions.add("MANAGE_DP_WALLET");
        permissions.add("ASSIGN_DP_MANAGER");
        permissions.add("VIEW_STATIC_PAGE");

        BouncerUser dummy = new BouncerUser();
        dummy.setUser(null);
        dummy.setUserPermissions(permissions);
        dummy.setAccessToken(null);
        dummy.setLastSyncWithBouncer(null);
        dummy.setUserId(0);
        return dummy;
    }

}
