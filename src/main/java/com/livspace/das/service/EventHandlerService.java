package com.livspace.das.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.livspace.das.events.EventWrapper;
import com.livspace.das.events.PublicEvent;
import com.livspace.das.events.SubscribeChannel;
import com.livspace.das.events.SubscribedEvents;
import com.livspace.das.exceptions.BadRequestException;
import com.livspace.das.exceptions.NotFoundException;
import com.livspace.das.exceptions.UnavailableException;
import com.livspace.event.common.models.EnvironmentType;
import com.livspace.event.wrapper.model.EventsServiceConfig;
import com.livspace.event.wrapper.model.StarMsEventHandler;
import com.livspace.event.wrapper.model.EventProcessResponse;
import com.livspace.event.common.models.StarMsEvent;
import com.livspace.event.wrapper.model.SystemType;
import com.livspace.event.wrapper.model.queue.AsyncQueue;
import com.livspace.event.wrapper.model.queue.ConsumerQueue;
import com.livspace.event.wrapper.model.queue.SyncQueue;
import com.livspace.event.wrapper.service.EventService;
import com.livspace.jobs.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by vishal on 17/12/18.
 */
@Service
public class EventHandlerService implements StarMsEventHandler {

    private static Logger logger = LoggerFactory.getLogger(EventHandlerService.class);
    private com.livspace.event.wrapper.service.EventService eventService;

    @Autowired
    @Qualifier("myObjectMapper")
    ObjectMapper mapper;

    @Autowired
    private Environment env;

    @Autowired
    private JobService jobService;

    @PostConstruct
    public void register() throws Exception{
        EventsServiceConfig eventsServiceConfig = new EventsServiceConfig();
        eventsServiceConfig.setSystem(SystemType.valueOf(env.getProperty("events.system")));

        EnvironmentType eEnv = null;
        try {
            eEnv = EnvironmentType.valueOf(env.getProperty("events.environment", "Dev"));
        } catch (Exception ex){}
        if(eEnv == null){
            eEnv = EnvironmentType.Dev;
        }

        boolean useDefaultQueue = Boolean.valueOf(env.getProperty("events.useDefaultQueue", "true"));
        logger.info("Event system using Env: " + eEnv);
        eventsServiceConfig.setEnv(eEnv);

        //Bind all the subscribed events
        List<String> syncBindings = new ArrayList<>();
        List<String> asyncBindings = new ArrayList<>();
        for(SubscribedEvents e : SubscribedEvents.values()){
            if(e.getChannel().equals(SubscribeChannel.Sync)) {
                syncBindings.add(e.getName());
            } else {
                asyncBindings.add(e.getName());
            }
        }

        SyncQueue syncQueue = new SyncQueue(syncBindings, this, useDefaultQueue);
        AsyncQueue asyncQueue = new AsyncQueue(asyncBindings,this,"async", 5);

        List<ConsumerQueue> list = new ArrayList<>();
        list.add(syncQueue);
        list.add(asyncQueue);
        eventsServiceConfig.setQueues(list);

        try {
            eventService = EventService.getInstance(eventsServiceConfig);
            eventService.listen();
            logger.info("Initiated the event service");
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error("Unable to initiate the event system");
        }

        if(eventService == null){
            throw new UnavailableException("Exception during connecting to the event system");
        }
    }

    @PreDestroy
    public void destroy(){
        if(eventService != null){
            eventService.shutdown();
        }
    }

    public void publish(PublicEvent event){
        try{
            String name = event.getEvent().toString();
            String payload = mapper.writeValueAsString(event.getData());
            StarMsEvent  sme = new StarMsEvent(name,payload);
            logger.info("Publishing Event: " + event.getEvent());
            logger.info("StarMS Event: " + sme.toString());
            eventService.publish(sme);
        }catch (Exception ex){
            ex.printStackTrace();
            logger.error("Unable to publish event " + event.toString());
        }
    }


    @Override
    public EventProcessResponse processEvent(StarMsEvent starMsEvent) {
        try {
            EventWrapper wrapped = new EventWrapper(starMsEvent, mapper);
            logger.debug(starMsEvent.toString());
            switch (SubscribedEvents.valueOf(wrapped.getEvent().getName())) {
                case CMS_SKU_CREATED:
                case CMS_SKU_UPDATED:
            }
        } catch (NotFoundException ex){
            logger.debug(ex.getMessage());
            return EventProcessResponse.Discarded;
        } catch (Exception ex){
            logger.error("EVENT_PROCESSING_FAILED: ", ex);
            throw new BadRequestException(ex);
        }
        return EventProcessResponse.Discarded;
    }

    private <T> EventProcessResponse processEvent(EventWrapper wrapped, Class<T> tClass,  Consumer<T> consumer) throws Exception{
        T event = wrapped.getPayloadAs(tClass);
        consumer.accept(event);
        return EventProcessResponse.Processed;
    }

    private <T,E> EventProcessResponse processEvent(EventWrapper wrapped, Class<T> tClass, Function<T,E> transform, Consumer<E> consumer) throws Exception{
        T event = wrapped.getPayloadAs(tClass);
        E transformed = transform.apply(event);
        consumer.accept(transformed);
        return EventProcessResponse.Processed;
    }

}
