package com.livspace.das.service;

import com.livspace.das.dao.BaseDao;
import com.livspace.das.dao.BoqProductDao;
import com.livspace.das.model.BoqProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoqProductService extends BaseService<BoqProduct>{

    @Autowired
    private BoqProductDao boqProductDao;

    @Override
    public BaseDao<BoqProduct> getDao() {
        return boqProductDao;
    }

    @Override
    public Class<BoqProduct> getTypeClass() {
        return BoqProduct.class;
    }
}
