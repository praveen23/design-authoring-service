package com.livspace.das.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.livspace.das.controllers.api.ResultMapper;
import com.livspace.das.model.BaseEntity;
import com.livspace.das.service.BaseService;
import com.livspace.das.util.JsonUtil;
import com.livspace.das.util.api.request.UrlParameters;
import com.livspace.das.util.api.response.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.invoke.MethodHandles;

public abstract class BaseController<T extends BaseEntity> {

    public abstract BaseService getService() ;

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    ResultMapper resultMapper;

    @Autowired
    JsonUtil jsonUtil;

    @GetMapping("{id}")
    public ResponseEntity get(@PathVariable("id") Long id, @RequestParam(value = "include", required = false) String include) {
        try {
            BaseEntity entity = include==null ? getService().find(id) : getService().find(id, UrlParameters.getInstance().buildInclude(include));
            return new ResponseEntity<>(entity, HttpStatus.OK);
        }catch (RuntimeException rex){
            logger.error("Error Getting " + getService().getTypeClass() + " entity with id: "+id, rex);
            return resultMapper.map(rex);
        }
    }

    @GetMapping()
    public ResponseEntity getAll(@RequestParam(value = "filter", required = false) String filter, @RequestParam(value = "include", required = false) String include,
                                       @RequestParam(value = "sort", required = false) String sort, @RequestParam(value = "start", required = false) Long start,
                                       @RequestParam(value = "count", required = false) Long count ) {
        try {
            UrlParameters urlParameters = new UrlParameters(filter, include, sort, start, count);
            Page page = getService().getAll(urlParameters);
            return new ResponseEntity<>(page, HttpStatus.OK);
        }catch (RuntimeException rex){
            logger.error("Error Getting " + getService().getTypeClass() + " entity with filters: "+ filter, rex);
            return resultMapper.map(rex);
        }
    }

    /**
     * Creating a Search endpoint for massive get requests,
     * @param urlParameters
     * @return
     */
    @PostMapping("/search")
    public ResponseEntity search(@RequestBody UrlParameters urlParameters) {
        try {
            Page page = getService().getAll(urlParameters);
            return new ResponseEntity<>(page, HttpStatus.OK);
        }catch (RuntimeException rex){
            logger.error("Error Getting " + getService().getTypeClass() + " entity with filters: "+ urlParameters.getFilter().toString(), rex);
            return resultMapper.map(rex);
        }
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody T entity) {
        try {
            BaseEntity entitySaved = getService().save(entity);
            return new ResponseEntity<>(entitySaved, HttpStatus.CREATED);
        }catch (RuntimeException rex){
            logger.error("Error Adding " + getService().getTypeClass() + " entity  :"+ jsonUtil.toJson(entity), rex);
            return resultMapper.map(rex);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody JsonNode jsonData) {
        try {
            BaseEntity entity = getService().update(id, jsonData);
            return new ResponseEntity<>(entity, HttpStatus.OK);
        }catch (RuntimeException rex){
            logger.error("Error Editing " + getService().getTypeClass() + " entity with id: "+ id + " and Json : "+ jsonData, rex);
            return resultMapper.map(rex);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        try {
            getService().delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (RuntimeException rex){
            logger.error("Error Deleting " + getService().getTypeClass() + " entity with id: "+ id, rex);
            return resultMapper.map(rex);
        }
    }
}
