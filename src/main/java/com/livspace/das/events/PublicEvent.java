package com.livspace.das.events;

public class PublicEvent {
    private Event event;
    private Object data;

    public PublicEvent(Event event, Object data) {
        this.event = event;
        this.data = data;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PublicEvent{" +
                "event=" + event +
                ", data=" + data +
                '}';
    }
}
