package com.livspace.das.events;

public interface Event {
    ;
    public String getName();
    public EventType getType();
}
