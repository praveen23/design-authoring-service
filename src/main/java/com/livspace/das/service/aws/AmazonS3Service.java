package com.livspace.das.service.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.livspace.das.exceptions.BadRequestException;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.UUID;

/**
 * Created by Abhinav on 9/17/15.
 */
@Service
public class AmazonS3Service {

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${aws.access.key}")
    private String awsAccessKey;

    @Value("${aws.secret.key}")
    private String awsSecretKey;

    @Value("${s3.bucket.name}")
    private String awsS3Bucket ;

    @Value("${s3.bucket.base}")
    private String bucketBase;

    private static final String linuxSep = "/";

    public String upload(File uploadFile, String key, ObjectMetadata metadata, CannedAccessControlList acl) {
        try {
            AWSCredentials   awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
            AmazonS3         s3Client       = new AmazonS3Client(awsCredentials);
            PutObjectRequest putRequest     = new PutObjectRequest(awsS3Bucket, key, uploadFile);

            if(metadata != null)putRequest.setMetadata(metadata);
            if(acl != null )putRequest.withCannedAcl(acl);
            s3Client.putObject(putRequest);
            return key;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException("AWS S3: Error uploading file with key: " + key);
        }
    }

    public String upload(InputStream stream, String key, ObjectMetadata metadata, CannedAccessControlList acl) {
        try {

            if(metadata == null) {
                metadata = new ObjectMetadata();
            }
            byte[] bytes = IOUtils.toByteArray(stream);
            metadata.setContentLength(bytes.length);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

            AWSCredentials   awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
            AmazonS3         s3Client       = new AmazonS3Client(awsCredentials);
            PutObjectRequest putRequest     = new PutObjectRequest(awsS3Bucket, key, byteArrayInputStream, null);

            if(metadata != null)putRequest.setMetadata(metadata);
            if(acl != null )putRequest.withCannedAcl(acl);
            s3Client.putObject(putRequest);
            return key;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException("AWS S3: Error uploading file with key: " + key,ex);
        }
    }

    public InputStream download(String bucketPath){
        if(bucketPath == null)
            throw new BadRequestException("Cannot download null file.");
        AWSCredentials awsCredentials   = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        AmazonS3       s3Client         = new AmazonS3Client(awsCredentials);
        InputStream    objectDataStream = null;
        try {
            S3Object object = s3Client.getObject(new GetObjectRequest(awsS3Bucket, bucketPath));
            objectDataStream = object.getObjectContent();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException("AWS S3: Error downloading file with path: " + bucketPath);
        }
        return objectDataStream;
    }

    public String getURL(String key) {
        if(key==null || key.trim().length()==0)
            return null;
        AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        AmazonS3Client s3Client       = new AmazonS3Client(awsCredentials);
        String url = s3Client.getUrl(awsS3Bucket, key).toString();
        return url.replaceFirst("https://","http://");
    }

    public String createKey(UploadClass uploadClass, String fileName){
        String[] fileNameSplit = fileName.split("\\.");
        String extension = fileNameSplit[fileNameSplit.length -1];
        StringBuilder sb = new StringBuilder(bucketBase) ;
        sb.append(linuxSep).append(uploadClass.toString().toLowerCase());
        sb.append(linuxSep); sb.append(fileName.trim().replaceAll(" ","_"));
        sb.append(linuxSep); sb.append(UUID.randomUUID()); // This supports multiple file uploads with same fileNames.
        sb.append(".");
        sb.append(extension);
        return sb.toString();
    }

    public enum UploadClass{
        DOCUMENTS
    }
}

