package com.livspace.das.util.web;

import com.amazonaws.util.IOUtils;
import com.livspace.das.exceptions.BadRequestException;
import com.livspace.das.exceptions.UnavailableException;
import com.livspace.das.service.security.SecurityContextHolder;
import com.livspace.das.util.JsonUtil;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
public class CommonWebUtil {

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    final static Map<String, String> AXLE_HEADERS = new HashMap<>();

    final static Map<String, String> CONTENT_HEADER = new HashMap<>();

    final static long WAIT = 30L;

    private final static Map<String,String> bouncerHeaders = new HashMap<>();

    @Autowired
    private JsonUtil jsonUtil;

    @Autowired
    private Environment env;

    private String bouncerClientId;
    private String bouncerClientSecret;

    @PostConstruct
    public void initHeaders(){
        CONTENT_HEADER.put("Content-Type","application/json");

        if(AXLE_HEADERS.size() != 0 )
        return;
        try {
            bouncerClientSecret = env.getProperty("bouncer.client.secret");
            bouncerClientId = env.getProperty("bouncer.client.id");
            bouncerHeaders.put("X-CLIENT-ID", bouncerClientId);
            bouncerHeaders.put("X-CLIENT-SECRET", bouncerClientSecret);

            AXLE_HEADERS.put("x-client-id", env.getProperty("axle.x-client-id"));
            AXLE_HEADERS.put("x-requested-by", env.getProperty("axle.x-requested-by"));
            AXLE_HEADERS.put("x-client-secret", env.getProperty("axle.x-client-secret"));
        }catch (Exception ex){
            logger.error("Error initializing CommonWebUtil, expect weird behaviour",ex);
        }
    }

    public Map<String,String> getAxleHeaders(){
        if( SecurityContextHolder.bouncerContext != null && SecurityContextHolder.bouncerContext.get() != null ){
            String requestedBy =  SecurityContextHolder.bouncerContext.get().getUserId() + "";
            AXLE_HEADERS.put("x-requested-by",requestedBy);
        }
        return AXLE_HEADERS;
    }

    private JsonNode invoke(String url, Map<String,String> headers, HttpMethod httpMethod, Object postData ){
        HttpResponse<JsonNode> jsonResponse = null;
        JsonNode postJson;
        try {
            switch (httpMethod) {
                case GET:
                    jsonResponse = Unirest.get(url).headers(headers).asJson();
                    break;
                case PUT:
                    postJson = new JsonNode(jsonUtil.toJsonString(postData));
                    headers.putAll(CONTENT_HEADER);
                    jsonResponse = Unirest.put(url).headers(headers).body(postJson).asJson();
                    break;
                case POST:
                    postJson = new JsonNode(jsonUtil.toJsonString(postData));
                    headers.putAll(CONTENT_HEADER);
                    jsonResponse = Unirest.post(url).headers(headers).body(postJson).asJson();
                    break;
                case DELETE:
                    jsonResponse = Unirest.delete(url).headers(headers).asJson();
                    break;
                default: Unirest.get(url).headers(headers).asJson();
            }
        }catch (UnirestException uex){
            throw new UnavailableException("Error calling URL :" + url + " with headers"+ headers+" , method : "+ httpMethod+ " payload : "+ postData, uex);
        }

        return ( jsonResponse == null ? null : jsonResponse.getBody() ) ;

    }

    public com.fasterxml.jackson.databind.JsonNode invokeUrl(String url,  Map<String,String> headers, HttpMethod httpMethod, Object postData, int retryCount){
        com.fasterxml.jackson.databind.JsonNode resultJson = null;
        for(int i=0; i<retryCount; i++){
            try {
                JsonNode jsonNode = invoke(url, headers, httpMethod, postData);
                String jsonString  = jsonNode.toString();
                resultJson = jsonUtil.parse(jsonString);
                return resultJson;
            }catch (RuntimeException cue){
                logger.info("Exception for retry count : "+ i+1 + " " + (retryCount - i + 1) +" retries left. " + cue.getMessage(), cue);
                if( i+1 == retryCount )
                    throw cue;
            }
        }
        throw new UnavailableException("Error invoking external url after "+retryCount+" retries " );
    }

    public HttpResponse<InputStream> axleProxy(HttpServletRequest servletRequest){
        String url = env.getProperty("axle.host") + servletRequest.getServletPath();
        url += (servletRequest.getQueryString() == null) ? "": "?" + servletRequest.getQueryString();
        String method = servletRequest.getMethod();
        Map<String, String> headers = new HashMap<>();
        headers.putAll(getAxleHeaders());
        return invokeProxy(method, servletRequest, url,headers);

    }

    public HttpResponse<InputStream> bouncerProxy(HttpServletRequest servletRequest){
        String url = env.getProperty("bouncer.host") + servletRequest.getServletPath().replaceAll("/proxy/bouncer","");
        url += (servletRequest.getQueryString() == null) ? "": "?" + servletRequest.getQueryString();
        String method = servletRequest.getMethod();
        Map<String, String> headers = new HashMap<>();
        headers.putAll(bouncerHeaders);
        return invokeProxy(method, servletRequest, url,headers);
    }

    private HttpResponse<InputStream> invokeProxy(String method, HttpServletRequest servletRequest, String url, Map<String, String> headers){
        try {

            switch (method) {
                case "GET":
                    return Unirest.get(url).headers(headers).asBinaryAsync().get(WAIT, TimeUnit.SECONDS);

                case "PUT":
                    headers.putAll(CONTENT_HEADER);
                    return Unirest.put(url).headers(headers).body(IOUtils.toString(servletRequest.getInputStream())).asBinaryAsync().get(WAIT, TimeUnit.SECONDS);

                case "POST":
                    headers.putAll(CONTENT_HEADER);
                    return Unirest.post(url).headers(headers).body(IOUtils.toString(servletRequest.getInputStream())).asBinaryAsync().get(WAIT, TimeUnit.SECONDS);

                case "DELETE":
                    return Unirest.delete(url).headers(headers).asBinaryAsync().get(WAIT, TimeUnit.SECONDS);

                default: throw new BadRequestException("Unsupported method:"+method);
            }
        }catch (TimeoutException |ExecutionException |InterruptedException|IOException ioe){
            throw new UnavailableException("Error executing proxy controller for url:"+ url, ioe);
        }
    }


}
