package com.livspace.das.controllers.api;

import org.springframework.http.HttpStatus;

public class ApiResponse {
    public enum ResponseStatus{
        SUCCESS,FAILED
    }
    protected ResponseStatus status;
    protected HttpStatus     statusCode;
    protected String         statusMessage;

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
