package com.livspace.das.service.security;

import com.bouncer.client.BouncerClient;
import com.bouncer.model.User;
import com.bouncer.model.UserPermission;
import com.bouncer.model.UserResponse;
import com.livspace.das.exceptions.UnavailableException;
import com.livspace.das.util.JsonUtil;
import com.livspace.das.util.security.BouncerUser;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityService {

    @Autowired
    private Environment env;

    @Autowired
    JsonUtil jsonUtil;

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final Integer COOKIE_EXPIRE_AFTER = 86400;
    private static final Integer BOUNCER_VALIDATE_AFTER = 30 * 60 * 1000;
    private static final String BOUNCER_SECRET_KEY = "WTRNUFk1UEIzOVlLN09NOTRRRkhWTTNBTlBHWDhINlU=";
    private static final String ALGO_MODE = "AES/CBC/PKCS5Padding";


    private String BOUNCER_CLIENT_SECRET;
    private String BOUNCER_CLIENT_ID;
    private String BOUNCER_HOST;
    private String COOKIE;

    private Cipher cipher;
    private BouncerClient client;
    private SecretKeySpec keySpec;
    int ivSize = 16;

    @PostConstruct
    public void init(){
        try{
            BOUNCER_HOST = env.getProperty("bouncer.host");
            BOUNCER_CLIENT_ID = env.getProperty("bouncer.client.id");
            BOUNCER_CLIENT_SECRET = env.getProperty("bouncer.client.secret");

            COOKIE = env.getProperty("das.cookie.name");
            client = new BouncerClient(BOUNCER_HOST, BOUNCER_CLIENT_ID, BOUNCER_CLIENT_SECRET);

            byte[] aesKey = Base64.decodeBase64(BOUNCER_SECRET_KEY);
            keySpec =  new SecretKeySpec(aesKey, "AES");
            cipher = Cipher.getInstance(ALGO_MODE);

        }catch (Exception e){
            logger.error("Error initializing Security Service",e);
            throw new UnavailableException(e);
        }
    }


    private  String decryptInCBC(String encryptedData) throws Exception {
        byte[] decodedValue = Base64.decodeBase64(encryptedData);

        // Extract IV.
        byte[] iv = new byte[ivSize];
        System.arraycopy(decodedValue, 0, iv, 0, iv.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, keySpec,ivParameterSpec);

        int encryptedSize = decodedValue.length - ivSize;
        byte[] encryptedBytes = new byte[encryptedSize];
        System.arraycopy(decodedValue, ivSize, encryptedBytes, 0, encryptedSize);

        byte[] decValue = cipher.doFinal(encryptedBytes);
        return new String(decValue);
    }

    private  String encryptInCBC(String data) throws Exception {
        byte[] ivByte = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(ivByte);
        IvParameterSpec iv = new IvParameterSpec(ivByte);

        cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
        byte[] encVal = cipher.doFinal(data.getBytes());

        byte[] encryptedIVAndText = new byte[ ivSize + encVal.length];

        System.arraycopy(ivByte, 0, encryptedIVAndText, 0, ivSize); // Copy iv to merged array
        System.arraycopy(encVal, 0, encryptedIVAndText, ivSize, encVal.length); // Copy encrypted value to merged.

        return Base64.encodeBase64String(encryptedIVAndText);
    }

    private BouncerUser decrypt(String encryptedData){
        try {
            String decryptedValue = decryptInCBC(encryptedData);
            return jsonUtil.fromJson(jsonUtil.parse(decryptedValue), BouncerUser.class);
        } catch (Exception ex) {
            logger.error(ex.getClass().getCanonicalName() + ex.getMessage());
            throw new RuntimeException("Cookie decryption failed", ex);
        }
    }

    private   String encrypt(BouncerUser user) {
        try {
            String json = jsonUtil.toJsonString(user);
            return encryptInCBC(json);
        } catch( Exception ex) {
            logger.error(ex.getClass().getCanonicalName() + ex.getMessage());
            throw new RuntimeException(COOKIE + " Cookie encryption failed",ex);
        }
    }

    public   BouncerUser getBouncerUserFromRequest(HttpServletRequest request) {
        Cookie  cookie = WebUtils.getCookie(request,COOKIE);
        if( cookie == null)
            return null;
        String cookieVal = cookie.getValue();
        return (cookieVal == null ? null : decrypt(cookieVal));
    }

    public void setSecurityContext(String requestedBy){
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(getBouncerUserFromId(requestedBy), null );
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    public BouncerUser getBouncerUserFromId(String id) {
        BouncerUser bouncerUser = new BouncerUser();
        bouncerUser.setUserId(Integer.parseInt(id));
        bouncerUser.setImageUrl("http://cdn.livmatrix.com/ls_image/663887/logo3.png");
        bouncerUser.setUser( new User());
        try {
            if(bouncerUser.getUserId() != 0){
                List<UserPermission> userPermissions = client.getUserPermissionsForApp(new Long(id), BOUNCER_CLIENT_ID );
                bouncerUser.setUserPermissions(userPermissions.stream().map(UserPermission::getName).collect(Collectors.toSet()));
            }else {
                // API call simulated using 0 as id:
                bouncerUser = BouncerUser.getDummyUser();
            }
        }catch (Exception ex){
            logger.error("Error getting permissions for bouncer id : " + bouncerUser.getUserId());
        }
        return bouncerUser;
    }

    private   void discardCookie(HttpServletResponse response){
        Cookie cookie = new Cookie(COOKIE, "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    public   BouncerUser validateBouncerUser(HttpServletResponse response, BouncerUser bouncerUser) {
        Date currentDate= new Date();
        Timestamp currentTimestamp = new Timestamp(currentDate.getTime());

        Date lastSyncDateWithBouncer = bouncerUser.getLastSyncWithBouncer();
        Timestamp lastSyncWithBouncer = new Timestamp(lastSyncDateWithBouncer.getTime());

        if( currentTimestamp.getTime() - lastSyncWithBouncer.getTime() > BOUNCER_VALIDATE_AFTER) {

            try {
                String accessToken = bouncerUser.getAccessToken();
                User user = client.validateAccessToken(accessToken);
                if ( user == null ) {
                    discardCookie(response);
                    logger.error("Unable to retrieve user details from Bouncer - Invalid access token");
                    return null;
                }

                // Fetching User permissions
                List<UserPermission> userPermissions = client.getUserPermissionsForApp(user.getId(),BOUNCER_CLIENT_ID );
                if(userPermissions==null || userPermissions.size() == 0){
                    discardCookie(response);
                    logger.error("No user permissions found for this user on application: "+BOUNCER_CLIENT_ID);
                    return null;
                }

                bouncerUser.getUserPermissions().addAll(
                        userPermissions.stream().map(UserPermission::getName).collect(Collectors.toList())
                );

                bouncerUser.setUser(user);
                bouncerUser.setUserId(user.getId().intValue());
                bouncerUser.setLastSyncWithBouncer(currentDate);
                bouncerUser.setAccessToken(accessToken);

                Cookie cookie = new Cookie(COOKIE, encrypt(bouncerUser));
                cookie.setMaxAge(COOKIE_EXPIRE_AFTER);
                response.addCookie(cookie);
                return bouncerUser;

            } catch (Exception ex) {
                logger.error(ex.getClass().getCanonicalName() + ex.getMessage());
                throw new RuntimeException("Client Login Failed");
            }
        }

        return bouncerUser;
    }

    public BouncerUser handleLogin(String accessCode, HttpServletResponse response ) throws RuntimeException {
        try {

            UserResponse  userResponse = client.getAccessToken(accessCode);
            String        accessToken  = userResponse.getAccessToken();
            User          user         = client.validateAccessToken(accessToken);
            if ( user == null ) {
                Cookie cookie = new Cookie(COOKIE,null);
                cookie.setMaxAge(0);
                response.addCookie(cookie);
                logger.error("Unable to retrieve user details from Bouncer - Invalid access token");
                return null;
            }
            // Fetching User permissions

            List<UserPermission> userPermissions = null;
            try {
                userPermissions = client.getUserPermissionsForApp(user.getId(), BOUNCER_CLIENT_ID);
            } catch (NullPointerException npe){
                logger.info("No permissions found for this user for Design Authering Service");
            }
            // Here we are allowing anyone with bouncer login to view static page.
            BouncerUser bouncerUser = new BouncerUser();
            if( userPermissions == null || userPermissions.size() == 0)
                bouncerUser.getUserPermissions().add("VIEW_STATIC_PAGE");
            else
                bouncerUser.getUserPermissions().addAll(userPermissions.stream().map(UserPermission::getName).collect(Collectors.toList()));

            bouncerUser.setUserId(user.getId().intValue());
            bouncerUser.setImageUrl(user.getImageUrl());
            Date date= new Date();
            bouncerUser.setLastSyncWithBouncer(date);
            bouncerUser.setAccessToken(accessToken);

            Cookie cookie = new Cookie(COOKIE,encrypt(bouncerUser));
            cookie.setMaxAge(COOKIE_EXPIRE_AFTER);
            response.addCookie(cookie);
            return bouncerUser;

        } catch(Exception ex) {
            logger.error(ex.getClass().getCanonicalName() + ex.getMessage());
            throw new RuntimeException("Client Login Failed",ex);
        }
    }

}
