package com.livspace.das.controllers.api;

import com.livspace.das.exceptions.GeneralException;
import com.livspace.das.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


/**
 * Created by Abhinav on 9/29/15.
 */
@Component
public class ResultMapper {

    @Autowired
    JsonUtil jsonUtil;

    public ResponseEntity map(RuntimeException ex){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(ApiResponse.ResponseStatus.FAILED);
        apiResponse.setStatusMessage(ex.getMessage()==null?"":ex.getMessage());
        if(ex instanceof GeneralException){
            HttpStatus statusCode = ((GeneralException)ex).getStatus();
            apiResponse.setStatusCode(statusCode);
        }else{
            HttpStatus statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
            apiResponse.setStatusCode(statusCode);
        }
        return ResponseEntity.status(apiResponse.getStatusCode()).body(apiResponse);
    }

}
