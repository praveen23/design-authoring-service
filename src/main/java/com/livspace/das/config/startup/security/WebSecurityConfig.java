package com.livspace.das.config.startup.security;

import com.livspace.das.config.startup.security.filter.CookieAuthFilter;
import com.livspace.das.config.startup.security.filter.TokenAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    TokenAuthFilter tokenFilter;

    @Autowired
    CookieAuthFilter cookieAuthFilter;

    @Autowired
    CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.eraseCredentials(false);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .antMatcher("/api/**").authorizeRequests() //
                .anyRequest().authenticated() //
                .and()
                .addFilterBefore(tokenFilter, BasicAuthenticationFilter.class)
                .addFilterBefore(cookieAuthFilter,BasicAuthenticationFilter.class);
    }

}
