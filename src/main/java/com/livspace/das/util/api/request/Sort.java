package com.livspace.das.util.api.request;

import com.google.common.base.CaseFormat;
import com.livspace.das.exceptions.BadRequestException;

import javax.persistence.criteria.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ashish on Jul, 2018.
 */
public class Sort {

    private String sortString;
    private Map<String,String> sortValues;

    public Sort(String sortString) {
        this.sortString = sortString;
        sortValues = getSortTokens(sortString);
    }

    private Map<String,String> getSortTokens(String sortString){
        Map<String,String> map = new LinkedHashMap<>();
        if(sortString == null)
            return map;
        try {
            String[] filterArray = sortString.split(";");
            for (String filter : filterArray) {
                String[] splitKeyVal = filter.split(":");
                map.put(splitKeyVal[0], splitKeyVal[1] );
            }
        }catch(Exception ex){
            //Logger.trace("Error Parsing sort fields for sort query parameter: " + sortString);
            throw new BadRequestException("Error Parsing sort tokens.",ex);
        }
        return map;
    }

    public <T> void applyOrderBy(CriteriaBuilder cb, Root<T> root, CriteriaQuery query){
        if(sortValues != null && sortValues.size() > 0) {
            for(Map.Entry<String, String> entry : sortValues.entrySet() ){
                String[] keyArray = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, entry.getKey()).split("\\.");
                Path<T> path = root ;
                for(String s:keyArray)
                    path = path.get(s);
                Order order = entry.getValue().equalsIgnoreCase("asc")?cb.asc(path):cb.desc(path);
                query.orderBy(order);
            }
        }
    }
}
