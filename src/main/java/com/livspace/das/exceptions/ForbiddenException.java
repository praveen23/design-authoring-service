package com.livspace.das.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public class ForbiddenException extends GeneralException {

    public static final HttpStatus status = HttpStatus.FORBIDDEN;

    public ForbiddenException(String message){
        super(message);
    }

    public ForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenException(Throwable cause) {
        super(cause);
    }

    public HttpStatus getStatus() {
        return status;
    }

}
