package com.livspace.das.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CaseFormat;
import com.livspace.das.dao.BaseDao;
import com.livspace.das.exceptions.BadRequestException;
import com.livspace.das.exceptions.ConflictException;
import com.livspace.das.exceptions.NotFoundException;
import com.livspace.das.model.BaseEntity;
import com.livspace.das.model.BaseModel;
import com.livspace.das.util.JsonUtil;
import com.livspace.das.util.api.request.UrlParameters;
import com.livspace.das.util.api.response.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

@Transactional
public abstract class BaseService<T extends BaseEntity>{

    @Autowired
    JsonUtil jsonUtil;

    @Autowired
    @Qualifier("myObjectMapper")
    ObjectMapper mapper;

    public abstract BaseDao<T> getDao() ;

    public abstract Class<T> getTypeClass() ;

    public T save(T entity) {
        return getDao().save(entity);
    }

    public T find(Long id, UrlParameters urlParameters) {
        T  instance =  getDao().find(getTypeClass(), id);
        attachEntity(instance, urlParameters);
        return instance;
    }

    public T find(Long id) {
        T  instance =  getDao().find(getTypeClass(), id);
        return instance;
    }

    public List<T> list() {
        return getDao().list(getTypeClass());
    }

    public T update(Long id, JsonNode jsonData){
        return getDao().edit(applyJson(id, jsonData));
    }

    public T update(T entity){
        return getDao().edit(entity);
    }

    public String delete(T entity) {
            return getDao().delete( entity);
    }

    public String delete(Long id) {
        return getDao().delete(getTypeClass(), id);
    }

    public Page<T> getAll(UrlParameters urlParameters){
        Page<T> page = getDao().getAll(getTypeClass(), urlParameters);
        for (T records: page.getRecords()){
            attachEntity(records, urlParameters);
        }
        return page;
    }

    protected T applyJson(Long id, JsonNode jsonData){
        T dbModel  = getDao().find(getTypeClass(), id);
        Set<String> allowedFields = dbModel.allowedFields();
        try{
            if(dbModel == null)
                throw new NotFoundException("No model element found in DB for id:" + id);
            Field[] fields = dbModel.getClass().getDeclaredFields();
            for(Field field: fields){
                String fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,field.getName());
                if( allowedFields !=null && !allowedFields.contains(fieldName))
                    continue;
                if( jsonData.get(fieldName) != null){
                    Object obj = jsonUtil.fromJson(jsonData.get(fieldName), field.getType() );
                    field.setAccessible(true);
                    if( obj instanceof BaseModel){
                        BaseModel subItem = (BaseModel)obj;
//                        obj = getDao().find(field.getType(), subItem.getId());
                    }
                    field.set(dbModel, obj);
                }
            }
        }catch(IllegalAccessException iae){
            throw new ConflictException("Error setting field value for model: "+ getTypeClass(), iae);
        }catch (RuntimeException ex){
            throw new BadRequestException("Error editing the model: "+getTypeClass()+" data with id: "+id, ex );
        }
        return dbModel;
    }

    protected T applyJson(JsonNode jsonData) {
        T newEntity = jsonUtil.fromJson(jsonData, getTypeClass());
        if (jsonData.get("id") != null && !jsonData.get("id").asText().isEmpty()) {
            Field[] newEntityFields = newEntity.getClass().getDeclaredFields();
            Hashtable newHT = fieldsToHT(newEntityFields, newEntity);

            T oldEntity = find(jsonData.get("id").asLong());
            Class oldEntityClass = oldEntity.getClass();
            Field[] oldEntityFields = oldEntityClass.getDeclaredFields();

            for (Field field : oldEntityFields) {
                field.setAccessible(true);
                Class t = field.getType();
                Object newFieldValue = newHT.get(field.getName());
                String jsonFieldName = field.getName().replaceAll("(.)(\\p{Upper})", "$1_$2").toLowerCase();
                if (jsonData.get(jsonFieldName) != null) {
                    try {
                        Field fieldName = oldEntityClass.getDeclaredField(field.getName());
                        fieldName.setAccessible(true);
                        fieldName.set(oldEntity, newFieldValue);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    }
                }
            }
            return oldEntity;
        } else {
            return newEntity;
        }
    }

    private static Hashtable<String, Object> fieldsToHT(Field[] fields, Object obj){
        Hashtable<String,Object> hashtable = new Hashtable<>();
        for (Field field: fields){
            field.setAccessible(true);
            try {
                Object retrievedObject = field.get(obj);
                if (retrievedObject != null){
                    hashtable.put(field.getName(), field.get(obj));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return hashtable;
    }

    protected void attachEntity(T entity, UrlParameters urlParameters){

    }

}
