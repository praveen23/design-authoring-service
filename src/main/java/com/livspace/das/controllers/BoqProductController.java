package com.livspace.das.controllers;

import com.livspace.das.model.BoqProduct;
import com.livspace.das.service.BaseService;
import com.livspace.das.service.BoqProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2/skus")
public class BoqProductController extends BaseController<BoqProduct>{
    @Autowired
    private BoqProductService boqProductService;


    public BaseService getService(){
        return boqProductService;
    }
}