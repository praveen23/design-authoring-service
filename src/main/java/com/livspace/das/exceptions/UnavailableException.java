package com.livspace.das.exceptions;


import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public class UnavailableException extends GeneralException {

    public static final HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;

    public UnavailableException(String message){
        super(message);
    }

    public UnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailableException(Throwable cause) {
        super(cause);
    }

    public HttpStatus getStatus() {
        return status;
    }

}
