package com.livspace.das.config.startup.security;

import com.livspace.das.util.security.BouncerUser;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        BouncerUser user = (BouncerUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user !=null && user.getUserPermissions().size() > 0)
            return authentication;
        else
            return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals( UsernamePasswordAuthenticationToken.class);
    }
}
