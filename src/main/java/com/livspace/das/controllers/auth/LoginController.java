package com.livspace.das.controllers.auth;

import com.livspace.das.service.security.SecurityService;
import com.livspace.das.util.security.BouncerUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.lang.invoke.MethodHandles;

@Controller
public class LoginController {

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${das.cookie.name}")
    private String cookieName;

    @Value("${bouncer.host}")
    String bouncerUrl;

    @Value("${das.external.host}")
    String dasHost;

    @Autowired
    SecurityService securityService;

    @GetMapping("/login")
    public String login(@RequestParam(value = "next", required = false) String next) {
        if(next == null) next="";
        String url = bouncerUrl+"/login?next="+dasHost+"/handlelogin?next_url=" + next ;
        return "redirect:"+url;
    }

    @GetMapping("/logout")
    public void logout(HttpServletResponse response) {
        Cookie cookie = new Cookie(cookieName,null);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    @GetMapping("/handlelogin")
    public void handleLogin(HttpServletResponse response, @RequestParam(value = "next_url", required = false) String next, @RequestParam(value = "access_code") String accessCode) {
        BouncerUser user = securityService.handleLogin(accessCode,response);
        try {
            if (user != null)
                response.sendRedirect(next);
            else
                response.sendRedirect("/accessdenied");
        }catch (Exception ex){
            logger.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    @GetMapping("/accessdenied")
    public String accessDenied(){
        return "<h2>Access Denied. Please contact with Bouncer Admin for relevant roles and access<h2>";
    }

    @GetMapping("/currentuser")
    public ResponseEntity currentUser() {
        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.ok(object);
    }

}
