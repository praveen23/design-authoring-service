package com.livspace.das.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public class NotFoundException extends GeneralException {

    public static final HttpStatus status = HttpStatus.NOT_FOUND;

    public NotFoundException(String message){
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public HttpStatus getStatus() {
        return status;
    }
}