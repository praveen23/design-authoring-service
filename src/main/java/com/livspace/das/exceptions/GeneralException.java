package com.livspace.das.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public abstract class GeneralException extends RuntimeException{

    public GeneralException() {
        super();
    }

    public GeneralException(String message) {
        super(message);
    }

    public GeneralException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralException(Throwable cause) {
        super(cause);
    }

    abstract public HttpStatus getStatus();
}