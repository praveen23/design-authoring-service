package com.livspace.das.config.startup.security.filter;

import com.livspace.das.service.security.SecurityService;
import com.livspace.das.util.security.BouncerUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(3)
public class CookieAuthFilter extends OncePerRequestFilter {

    @Autowired
    SecurityService securityService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final HttpServletRequest httpRequest = request;

        // Check if header based auth has run
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getPrincipal() == null ){
            BouncerUser user = securityService.getBouncerUserFromRequest(httpRequest);

            if(user != null) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user,null );
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

                if( user.getUserPermissions().size() == 1 && user.getUserPermissions().contains("VIEW_STATIC_PAGE")){
                    if(request.getServletPath().startsWith("/pages") || request.getServletPath().startsWith("/api/v1/agreements/")
                                                                                    || request.getServletPath().startsWith("/logout"))
                        filterChain.doFilter(request, response);
                    else{
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }
                }
            }else{
                //Hack TODO: Remove this
                if(request.getServletPath().startsWith("/api") || request.getServletPath().startsWith("/proxy")){
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    return;
                }
                if(request.getServletPath().startsWith("/pages")){
                    response.sendRedirect("/login?next="+request.getRequestURL()+"?"+request.getQueryString());
                    return;
                }
            }
        }

        filterChain.doFilter(request, response);
    }

}
