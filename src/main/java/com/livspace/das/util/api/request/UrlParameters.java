package com.livspace.das.util.api.request;

import org.springframework.stereotype.Component;

@Component
public class UrlParameters {

    private Filter  filter = new Filter(null);
    private Include include = new Include(null);
    private Sort    sort = new Sort(null);
    private long    start = 0;
    private long    count = 25;

    private UrlParameters(){}

    public UrlParameters(String filterString, String includeString, String sortString, Long start, Long count) {
        this.buildFilter(filterString)
        .buildInclude(includeString)
        .buildSort(sortString)
        .buildStart(start)
        .buildCount(count);
    }

    public static UrlParameters getInstance(){
        return new UrlParameters();
    }
    // Builder Methods

    public UrlParameters buildFilter(String filter) {
        this.filter = new Filter(filter);
        return this;
    }

    public UrlParameters buildInclude(String include) {
        this.include = new Include(include);
        return this;
    }

    public UrlParameters buildSort(String sort) {
        this.sort = new Sort(sort);
        return this;
    }

    public UrlParameters buildStart(Long start) {
        if( start !=null )
            this.start = start;
        return this;

    }

    public UrlParameters buildCount(Long count) {
        if(count != null)
            this.count = count;
        return this;
    }

    // Getters and Setters


    public void setFilter(String filter) {
        this.buildFilter(filter);
    }

    public void setInclude(String include) {
        this.buildInclude(include);
    }

    public void setSort(String sort) {
        this.buildSort(sort);
    }

    public void setStart(long start) {
        this.buildStart(start);
    }

    public void setCount(long count) {
        this.buildCount(count);
    }

    public Filter getFilter() {
        return filter;
    }

    public Include getInclude() {
        return include;
    }

    public Sort getSort() {
        return sort;
    }

    public long getStart() {
        return start;
    }

    public long getCount() {
        return count;
    }
}
