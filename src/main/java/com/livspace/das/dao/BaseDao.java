package com.livspace.das.dao;

import com.livspace.das.exceptions.NotFoundException;
import com.livspace.das.model.BaseEntity;
import com.livspace.das.util.api.request.UrlParameters;
import com.livspace.das.util.api.response.Page;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class BaseDao<T extends BaseEntity> {


    @PersistenceContext
    protected EntityManager em;

    public T save(T entity) {
        em.persist(entity);
        return entity;
    }

    public T refresh(T entity){
        em.refresh(entity);
        return entity;
    }

    public <T> T find(Class<T> entityClass, long primaryKey) {
        T  instance  = em.find(entityClass, primaryKey);
        return instance;
    }

    public List<T> list(Class<T> entityClass) {
        return em.createQuery(String.format("select p from %s p", entityClass.getSimpleName()), entityClass).getResultList();
    }

    public <T> T edit(T entity) {
        em.merge(entity);
        return entity;
    }

    public String delete(T entity) {
        em.remove(entity);
        return String.format("deleted %s",
                entity.getClass().getName().substring(entity.getClass().getName().lastIndexOf('.') + 1));
    }

    public String delete(Class<T> entityClass, Long id) {
            T entity = find(entityClass, id);
            if(entity == null){
                throw new NotFoundException(String.format("Id %s not found for %s ",
                        id,
                        entityClass.getName().substring(entityClass.getName().lastIndexOf('.') + 1)
                ));
            }
            return delete(entity);
    }

    public Page<T>
    getAll(Class<T> entityClass, UrlParameters urlParameters){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> countRoot = countQuery.from(entityClass);
        countQuery.select(criteriaBuilder.count(countRoot));
        countQuery.where(urlParameters.getFilter().getPredicates(countRoot, criteriaBuilder, countQuery, true));
        Long count = em.createQuery(countQuery).getSingleResult();


        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<T> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root);
        criteriaQuery.where(urlParameters.getFilter().getPredicates(root, criteriaBuilder,criteriaQuery, true));
        // Order By
        urlParameters.getSort().applyOrderBy(criteriaBuilder, root, criteriaQuery);

        TypedQuery<T> typedQuery = em.createQuery(criteriaQuery);
        typedQuery.setFirstResult((int)urlParameters.getStart());
        typedQuery.setMaxResults((int)urlParameters.getCount());
        List<T> result = new ArrayList<>();
        try {
            result = typedQuery.getResultList();
        }catch(NoResultException nex){
            // do Nothing.
        }
        return new Page(urlParameters.getStart(), count, result);
    }
}
