package com.livspace.das.util.api.request;

import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.livspace.das.model.BaseModel;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.*;
import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ashish on Jul, 2018.
 */
public class Filter {

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private Map<String, HashSet<String>> filterMap;
    private String filterString;

    public Filter(String filterString) {
        this.filterString = filterString;
        this.filterMap = new HashMap<>();
        parseFilterMap();
    }

    public Map<String, HashSet<String>> getFilterMap() {
        return filterMap;
    }

    public void setFilterMap(Map<String, HashSet<String>> filterMap) {
        this.filterMap = filterMap;
    }

    public String getFilterString() {
        return filterString;
    }

    public void setFilterString(String filterString) {
        this.filterString = filterString;
    }

    private void parseFilterMap() {
        if (filterString == null)
            return;
        try {
            String[] filterArray = filterString.split(";");
            for (String filter : filterArray) {
                String[] splitFilter = filter.split(":");
                HashSet<String> filterVals = new HashSet<>();
                filterVals.addAll(Arrays.asList(splitFilter[1].split(",")));
                this.filterMap.put(splitFilter[0], filterVals);
            }
        } catch (Exception ex) {
            logger.trace("Error parsing filter String to HashMap " + ex.getMessage());
        }
    }

    @Override
    public String toString() {
        String filterStringTemp = new String();
        Iterator<Map.Entry<String,HashSet<String>>> iterator = filterMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String,HashSet<String>> entry = iterator.next();
            filterStringTemp +=  entry.getKey() + ":";
            filterStringTemp +=  Joiner.on(",").join(entry.getValue());
            filterStringTemp += ";" ;
        }
        this.filterString = filterStringTemp ;

        return filterString;
    }

    public void removeFilter(String key){
        if(this.filterMap.containsKey(key))
            this.filterMap.remove(key);
    }

    public void addFilter(String key, List<String> values) {
        HashSet<String> newFilter = new HashSet<>();
        newFilter.addAll(values);
        if (this.filterMap.containsKey(key))
            this.filterMap.get(key).addAll(newFilter);
        else
            this.filterMap.put(key, newFilter);
    }

    public void addFilter(String key, String... values) {
        HashSet<String> newFilter = new HashSet<>();
        newFilter.addAll(Arrays.asList(values));
        if (this.filterMap.containsKey(key))
            this.filterMap.get(key).addAll(newFilter);
        else
            this.filterMap.put(key, newFilter);
    }

    public void addFilter(String key, Integer... values) {
        ArrayList<String> valList = new ArrayList<>();
        String[] filterArray = new String[values.length];
        for (Integer value : values)
            valList.add(String.valueOf(value));
        addFilter(key, valList.toArray(filterArray));
    }

    public void addFilter(String key, Long... values) {
        ArrayList<String> valList = new ArrayList<>();
        String[] filterArray = new String[values.length];
        for (Long value : values)
            valList.add(String.valueOf(value));
        addFilter(key, valList.toArray(filterArray));
    }

    public ArrayList<Long> getFilterLong(String key) {
        ArrayList<Long> result    = new ArrayList<>();
        HashSet<String>    filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                result.addAll(filterVal.stream().mapToLong(Long::parseLong).boxed().collect(Collectors.toList()));
        } catch (Exception ex) {
            logger.debug("Error reading Integer for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    public ArrayList<Double> getFilterDouble(String key) {
        ArrayList<Double> result    = new ArrayList<>();
        HashSet<String>    filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                result.addAll(filterVal.stream().mapToDouble(Double::parseDouble).boxed().collect(Collectors.toList()));
        } catch (Exception ex) {
            logger.debug("Error reading Integer for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    public ArrayList<Integer> getFilterInt(String key) {
        ArrayList<Integer> result    = new ArrayList<>();
        HashSet<String>    filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                result.addAll(filterVal.stream().mapToInt(Integer::parseInt).boxed().collect(Collectors.toList()));
        } catch (Exception ex) {
            logger.debug("Error reading Integer for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    public boolean getFilterBool(String key) {
        HashSet<String> filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 1)
                throw new Exception("more than one values for boolean key: " + key);
            String value = filterVal.iterator().next();
            value = value.equals("1") ? "true" : "false";
            return Boolean.parseBoolean(value);
        } catch (NullPointerException nex) {
            logger.trace("Error reading Boolean for key: " + key + " :" + nex.getMessage());
        } catch (Exception ex) {
            logger.debug("Error reading Boolean for key: " + key + " :" + ex.getMessage());
        }
        return false;
    }

    public ArrayList<String> getFilterStr(String key) {
        ArrayList<String> result    = new ArrayList<>();
        HashSet<String>   filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                result.addAll(filterVal.stream().collect(Collectors.toList()));
        } catch (RuntimeException ex) {
            logger.debug("Error parsing filter for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    public ArrayList<DateTime> getFilterDateTime(Map<String, HashSet<String>> filters, String key) {
        ArrayList<DateTime> result    = new ArrayList<>();
        HashSet<String>   filterVal = filters.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                result.addAll(filterVal.stream().map(item -> DateTime.parse(item)).collect(Collectors.toList()));
        } catch (RuntimeException ex) {
            logger.debug("Error parsing filter for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    public ArrayList<Enum> getFilterEnum(Class<? extends Enum> enumType, String key) {
        ArrayList<Enum> result    = new ArrayList<>();
        HashSet<String>   filterVal = this.filterMap.get(key);
        try {
            if (filterVal != null && filterVal.size() > 0)
                filterVal.stream().forEach(value -> result.add(Enum.valueOf(enumType, value)));
        } catch (RuntimeException ex) {
            logger.debug("Error parsing filter for key: " + key + " :" + ex.getMessage());
        }
        return result;
    }

    private Path getPath(Path path, String entryKey){
        if( entryKey.startsWith("from_"))
            return  path.get( CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, entryKey.substring(5)) );
        if( entryKey.startsWith("to_"))
            return  path.get( CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, entryKey.substring(3)) );

        return path.get(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, entryKey ));
    }

    private Predicate strPredicateBuilder(String key, Path path, CriteriaBuilder criteriaBuilder, boolean wildCard){
        String padding = wildCard ? "%" : "";
        ArrayList<String> filterVal = getFilterStr(key);
        List<Predicate>  predicateList = new ArrayList<>();
        for(String filter : filterVal){
            predicateList.add( criteriaBuilder.like(path, padding + filter + padding  ));
        }
        if(predicateList.size() > 1 ) {
            return criteriaBuilder.or(predicateList.toArray(new Predicate[predicateList.size()]));
        }else
            return  predicateList.get(0) ;
    }

    public Predicate[] getPredicates(Root root, CriteriaBuilder criteriaBuilder, AbstractQuery query, boolean wildCard) {
        ArrayList<Predicate> predicates = new ArrayList<>();
        if (this.filterMap == null)
            return new Predicate[0];

        for (Map.Entry<String, HashSet<String>> entry : this.filterMap.entrySet()) {
            if (entry.getValue() == null || entry.getValue().size() == 0)
                continue;
            Path              path      = root;
            ArrayList<String> splitList = new ArrayList<>();
            for (String token : entry.getKey().split("\\."))
                splitList.add(token);

            try {
                Iterator<String> keysIterator = splitList.iterator();

                while (keysIterator.hasNext()) {
                    String key = keysIterator.next();
                    path = getPath(path, key);
                    if (BaseModel.class.isAssignableFrom(path.getJavaType())) {
                           if(keysIterator.hasNext())
                               predicates.add(getSubPredicates(criteriaBuilder, query, root, key,  keysIterator.next(),  entry.getValue(),wildCard));
                    } else {
                        buildNativePredicates(key, entry.getValue(), predicates, path, criteriaBuilder, wildCard );
                    }
                }
            } catch (IllegalArgumentException iae) {
                logger.info("illegal Argument : " + entry.getKey());
                continue;
            }
        }
        Predicate[] predicateArray = new Predicate[0];
        return predicates.toArray(predicateArray);
    }

    private Predicate getSubPredicates(CriteriaBuilder criteriaBuilder, AbstractQuery abstractQuery, Root root, String field, String key, HashSet<String> value, boolean wildCard ){
        Path path = root.get(field);
        ArrayList<Predicate> subQueryPredicates = new ArrayList<>();

        AbstractQuery abstractSubQuery = abstractQuery.subquery(path.getJavaType());
        Root subRootEntity = abstractSubQuery.from(path.getJavaType());

        Path subPath = subRootEntity;
        subPath = getPath(subPath, key);

        buildNativePredicates(key,value,subQueryPredicates,subPath,criteriaBuilder,wildCard);

        subQueryPredicates.add( criteriaBuilder.equal( root.get(field) ,subRootEntity));
        abstractSubQuery.where(subQueryPredicates.toArray(new Predicate[subQueryPredicates.size()]));
        Subquery subQuery = ((Subquery)abstractSubQuery).select(subRootEntity);
        return criteriaBuilder.exists(subQuery) ;

    }

    private void buildNativePredicates(String key, HashSet<String> value ,  ArrayList<Predicate> predicates, Path path, CriteriaBuilder criteriaBuilder,Boolean wildCard ){
        if ( value.contains("null")){
            predicates.add(path.isNull());
            return;
        }

        if ( value.contains("notnull")){
            predicates.add(path.isNotNull());
            return;
        }

        if( path.getJavaType().isEnum()){
            ArrayList<Enum> filterValuesList = getFilterEnum(path.getJavaType(), key);
            predicates.add(path.in(filterValuesList));
            return;
        }

        else if( path.getJavaType().isAssignableFrom(Boolean.class) || path.getJavaType().getName().equals("boolean")) {
            Boolean filterVal = getFilterBool(key);
            predicates.add(path.in(filterVal));
            return;
        }else if ( key.startsWith("from_") && path.getJavaType().isAssignableFrom(DateTime.class) ){
            predicates.add(criteriaBuilder.greaterThanOrEqualTo( path, new DateTime( value.iterator().next() ))) ;
            return;
        }
        else if ( key.startsWith("to_") && path.getJavaType().isAssignableFrom(DateTime.class) ){
            predicates.add(criteriaBuilder.lessThanOrEqualTo( path, new DateTime( value.iterator().next() ))) ;
            return;
        }
        else if( path.getJavaType().isAssignableFrom(String.class)) {
            predicates.add( strPredicateBuilder(key, path, criteriaBuilder, wildCard));
            return;
        }
        else
            predicates.add(path.in(value));
    }

}
