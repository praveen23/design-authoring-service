package com.livspace.das.exceptions;


import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public class BadRequestException extends GeneralException {

    public static final HttpStatus status = HttpStatus.BAD_REQUEST;

    public BadRequestException(String message){
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }

    public HttpStatus getStatus() {
        return status;
    }
}
