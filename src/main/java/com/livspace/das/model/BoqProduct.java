package com.livspace.das.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by Vishal on 9/26/16.
 */
@Entity
@Table(name = "boq_product")
@SQLDelete(sql = "UPDATE boq_product SET is_deleted = '1' WHERE id_customized_product = ?")
@Where(clause = "is_deleted <> '1'")
public class BoqProduct extends BaseEntity {
    private Long id;
    private String code;
    private String hsnCode;
    private BigDecimal price;
    private BigDecimal igst;
    private int idTaxRulesGroup = 1;
    private String name;
    private Integer categorySku;
    private Integer typeSku;
    private String filename;
    private String linkConstant;
    private String description;
    private String instructions;
    private Byte groupedSku = 0;
    private String skuDefinition;
    private String primaryCustomizedProduct;
    private byte assemblable = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customized_product", nullable = false, insertable = true, updatable = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code", nullable = true, insertable = true, updatable = true, length = 64)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "hsn_code", nullable = true, insertable = true, updatable = true, length = 25)
    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    @Basic
    @Column(name = "price", nullable = true, insertable = true, updatable = true, precision = 6)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "igst", nullable = true, insertable = true, updatable = true, precision = 4)
    public BigDecimal getIgst() {
        return igst;
    }

    public void setIgst(BigDecimal igst) {
        this.igst = igst;
    }

    @Basic
    @Column(name = "id_tax_rules_group", nullable = false, insertable = true, updatable = true)
    public int getIdTaxRulesGroup() {
        return idTaxRulesGroup;
    }

    public void setIdTaxRulesGroup(int idTaxRulesGroup) {
        this.idTaxRulesGroup = idTaxRulesGroup;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "category_sku", nullable = true, insertable = true, updatable = true)
    public Integer getCategorySku() {
        return categorySku;
    }

    public void setCategorySku(Integer categorySku) {
        this.categorySku = categorySku;
    }

    @Basic
    @Column(name = "type_sku", nullable = true, insertable = true, updatable = true)
    public Integer getTypeSku() {
        return typeSku;
    }

    public void setTypeSku(Integer typeSku) {
        this.typeSku = typeSku;
    }

    @Basic
    @Column(name = "filename", nullable = true, insertable = true, updatable = true, length = 2048)
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Basic
    @Column(name = "link_constant", nullable = false, insertable = true, updatable = false)
    public String getLinkConstant() {
        return linkConstant;
    }

    public void setLinkConstant(String linkConstant) {
        this.linkConstant = linkConstant;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "instructions", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    @Basic
    @Column(name = "grouped_sku", nullable = false, insertable = true, updatable = true)
    public Byte getGroupedSku() {
        return groupedSku;
    }

    public void setGroupedSku(Byte groupedSku) {
        this.groupedSku = groupedSku;
    }

    @Basic
    @Column(name = "sku_definition", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getSkuDefinition() {
        return skuDefinition;
    }

    public void setSkuDefinition(String skuDefinition) {
        this.skuDefinition = skuDefinition;
    }

    @Basic
    @Column(name = "primary_customized_product", nullable = true, insertable = true, updatable = true, length = 64)
    public String getPrimaryCustomizedProduct() {
        return primaryCustomizedProduct;
    }

    public void setPrimaryCustomizedProduct(String primaryCustomizedProduct) {
        this.primaryCustomizedProduct = primaryCustomizedProduct;
    }

    @Basic
    @Column(name = "assemblable", nullable = false, insertable = true, updatable = true)
    public byte getAssemblable() {
        return assemblable;
    }

    public void setAssemblable(byte assemblable) {
        this.assemblable = assemblable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BoqProduct that = (BoqProduct) o;

        if (idTaxRulesGroup != that.idTaxRulesGroup) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (categorySku != null ? !categorySku.equals(that.categorySku) : that.categorySku != null) return false;
        if (typeSku != null ? !typeSku.equals(that.typeSku) : that.typeSku != null) return false;
        if (filename != null ? !filename.equals(that.filename) : that.filename != null) return false;
        if (linkConstant != null ? !linkConstant.equals(that.linkConstant) : that.linkConstant != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (skuDefinition != null ? !skuDefinition.equals(that.skuDefinition) : that.skuDefinition != null) return false;
        return !(instructions != null ? !instructions.equals(that.instructions) : that.instructions != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + idTaxRulesGroup;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (categorySku != null ? categorySku.hashCode() : 0);
        result = 31 * result + (typeSku != null ? typeSku.hashCode() : 0);
        result = 31 * result + (filename != null ? filename.hashCode() : 0);
        result = 31 * result + (linkConstant != null ? linkConstant.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (instructions != null ? instructions.hashCode() : 0);
        result = 31 * result + (skuDefinition != null ? skuDefinition.hashCode() : 0);
        return result;
    }
}
