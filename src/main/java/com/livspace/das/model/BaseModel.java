package com.livspace.das.model;

import org.joda.time.DateTime;
import java.io.Serializable;
import java.sql.Timestamp;

public interface BaseModel extends Serializable {

    Timestamp getCreatedAt();
    void setCreatedAt(Timestamp createdAt);

    Timestamp getUpdatedAt();
    void setUpdatedAt(Timestamp updatedAt);
}