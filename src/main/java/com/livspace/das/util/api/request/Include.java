package com.livspace.das.util.api.request;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ashish on Jul, 2018.
 */
public class Include {

    private String includeString ;
    private Set<String> includeFields ;

    Include(String includeString){
        this.includeString = includeString;
        includeFields = getIncludeFields(includeString);
    }
    private Set<String> getIncludeFields(String include){
        Set<String> result = new HashSet<>();
        if(include == null)
            return result;
        result.addAll(Arrays.asList(include.split(",")));
        return result;
    }

    public String getIncludeString() {
        return includeString;
    }

    public void setIncludeString(String includeString) {
        this.includeString = includeString;
    }

    public Set<String> getIncludeFields() {
        return includeFields;
    }

    public void setIncludeFields(Set<String> includeFields) {
        this.includeFields = includeFields;
    }
}
