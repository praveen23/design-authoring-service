package com.livspace.das.service.security;

import com.livspace.das.util.security.BouncerUser;

public class SecurityContextHolder {
    public static InheritableThreadLocal<BouncerUser> bouncerContext = new InheritableThreadLocal<>();
    public static InheritableThreadLocal<String> requestId = new InheritableThreadLocal<>();

}
