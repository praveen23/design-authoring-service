package com.livspace.das.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.livspace.das.exceptions.BadRequestException;
import com.livspace.das.exceptions.NotFoundException;
import com.livspace.das.exceptions.UnavailableException;
import com.livspace.event.common.models.StarMsEvent;

public class EventWrapper {
    private ObjectMapper mapper;


    private StarMsEvent rawEvent;
    private Event event;

    public EventWrapper(StarMsEvent event, ObjectMapper mapper) throws NotFoundException {
        this.mapper = mapper;
        this.rawEvent = event;
        try{
            this.event = SubscribedEvents.valueOf(event.getName());
        }catch (Exception ex){
            throw new UnavailableException(event.getName());
        }
    }

    public <T> T getPayloadAs(Class<T> type) throws BadRequestException {
        try {
            return mapper.readValue(this.rawEvent.getData(), type);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new UnavailableException(this.rawEvent.getData());
        }
    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public StarMsEvent getRawEvent() {
        return rawEvent;
    }

    public void setRawEvent(StarMsEvent rawEvent) {
        this.rawEvent = rawEvent;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "EventWrapper{" +
                "event=" + event +
                '}';
    }
}
