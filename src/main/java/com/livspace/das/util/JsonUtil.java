package com.livspace.das.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.livspace.das.exceptions.UnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonUtil {

    @Autowired
    @Qualifier("myObjectMapper")
    ObjectMapper objectMapper;

    public JsonNode toJson(Object object){
        try{
            return objectMapper.convertValue(object, JsonNode.class);
        }catch (IllegalArgumentException iae){
            throw new UnavailableException("Error converting to Json: ", iae);
        }
    }

    public String toJsonString(Object object){
        try{
            return objectMapper.writeValueAsString(object);
        }catch (JsonProcessingException jpe){
            throw new UnavailableException("Error converting to Json: ", jpe);
        }
    }

    public JsonNode parse(String jsonData){
        try{
            return objectMapper.readValue(jsonData,JsonNode.class);
        }catch (IOException ex){
            throw  new UnavailableException("Error Parsing Json String to JsonNode.", ex);
        }
    }

    public <T> T fromJson(JsonNode jsonNode, Class<T> clazz){
        try{
            return objectMapper.treeToValue(jsonNode, clazz);
        }catch (JsonProcessingException jpe){
            throw  new UnavailableException("Error Parsing JsonNode to Class"+ clazz.getName(), jpe);
        }
    }

    public ObjectNode newObject(){
        return objectMapper.createObjectNode();
    }
}

