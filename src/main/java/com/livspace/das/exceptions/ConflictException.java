package com.livspace.das.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Created by ashish on Jul, 2018.
 */
public class ConflictException extends GeneralException {

    public static final HttpStatus status = HttpStatus.CONFLICT;

    public ConflictException(String message){
        super(message);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictException(Throwable cause) {
        super(cause);
    }

    public HttpStatus getStatus() {
        return status;
    }

}
