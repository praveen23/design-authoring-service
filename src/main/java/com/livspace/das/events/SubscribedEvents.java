package com.livspace.das.events;

public enum SubscribedEvents implements Event{
    CMS_SKU_CREATED(SubscribeChannel.Async),
    CMS_SKU_UPDATED(SubscribeChannel.Async);

    SubscribeChannel channel;

    SubscribedEvents(SubscribeChannel channel) {
        this.channel = channel;
    }

    SubscribedEvents() {
        this(SubscribeChannel.Sync);
    }

    @Override
    public String getName() {
        return this.toString();
    }

    @Override
    public EventType getType() {
        return EventType.SUBSCRIBING;
    }

    public SubscribeChannel getChannel(){
        return this.channel;
    }
}
