package com.livspace.das.util.web.bouncer;

import com.bouncer.model.User;
import com.fasterxml.jackson.databind.JsonNode;
import com.livspace.das.util.JsonUtil;
import com.livspace.das.util.web.CommonWebUtil;
import com.mashape.unirest.http.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
public class BouncerUtil {

    final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final static Map<String,String> bouncerHeaders = new HashMap<>();

    @Autowired
    CommonWebUtil commonWebUtil;

    @Autowired
    JsonUtil jsonUtil;

    @Autowired
    Environment environment;

    private String bouncerCreateUser;
    private String bouncerCitiesUrl;
    private String bouncerClientId;
    private String bouncerClientSecret;

    @PostConstruct
    public void initBouncerHeaders(){
        try {
            bouncerClientSecret = environment.getProperty("bouncer.client.secret");
            bouncerClientId = environment.getProperty("bouncer.client.id");
            bouncerCitiesUrl = environment.getProperty("bouncer.cities.url");
            bouncerCreateUser = environment.getProperty("bouncer.user.url");

            bouncerHeaders.put("X-CLIENT-ID", bouncerClientId);
            bouncerHeaders.put("X-CLIENT-SECRET", bouncerClientSecret);
        }catch (Exception ex){
            logger.error("Bouncer initiation did not happen properly , expect wierd behaviour", ex);
        }
    }

    public User getBouncerUserFromId(Long id){
        String url = bouncerCreateUser+"/"+id ;
        JsonNode jsonNode = commonWebUtil.invokeUrl(url, bouncerHeaders, HttpMethod.GET, null, 2);
        return jsonUtil.fromJson(jsonNode, User.class);
    }

    public HashMap<String, String> getUserBouncerUids(Collection<String> userBouncerIds){
        HashMap<String, String> userBouncerLoginMapper = new HashMap<>();
        if(userBouncerIds.size() == 0){
            return userBouncerLoginMapper;
        }
        String userFilterIds = String.join(",", userBouncerIds);
        String  url    = bouncerCreateUser + "?filters=id:" + userFilterIds + "&count=" + Integer.MAX_VALUE;
        JsonNode users = commonWebUtil.invokeUrl(url, bouncerHeaders, HttpMethod.GET, null, 2);
        Iterator<JsonNode> nodeIterator = users.get("items").iterator();
        while (nodeIterator.hasNext()) {
            JsonNode user = nodeIterator.next();
            userBouncerLoginMapper.put(user.get("id").asText(), user.get("uid").asText());
        }
        return userBouncerLoginMapper;
    }

}
