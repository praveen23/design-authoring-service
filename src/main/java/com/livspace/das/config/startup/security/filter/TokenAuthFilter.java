package com.livspace.das.config.startup.security.filter;

import com.livspace.das.service.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(2)
public class TokenAuthFilter extends OncePerRequestFilter
{
    @Value("${das.auth.token}")
    String xAuthToken;

    @Value("${auth.token.header}")
    String xAuthHeaderName;

    @Value("${auth.id.header}")
    String xRequestedBy;

    @Autowired
    SecurityService securityService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final HttpServletRequest httpRequest = request;

        //extract token from header
        final String accessToken = httpRequest.getHeader(xAuthHeaderName);
        if (accessToken !=null && accessToken.equals(xAuthToken)) {
            String requestedBy = httpRequest.getHeader(xRequestedBy);
            if(requestedBy == null)
                throw  new InternalAuthenticationServiceException("User cannot be null.");

            securityService.setSecurityContext(requestedBy);

        }

        filterChain.doFilter(request, response);
    }
}
